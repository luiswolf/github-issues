//
//  IssueTableViewCellTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class IssueTableViewCellTests: XCTestCase {

    func test_InitWithCoder_isNil() {
        let cell = IssueTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }

}
