//
//  StateTableViewCellTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class StateTableViewCellTests: XCTestCase {

    func test_InitWithCoder_isNil() {
        let cell = StateTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }

}
