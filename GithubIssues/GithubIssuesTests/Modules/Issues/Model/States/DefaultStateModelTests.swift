//
//  DefaultStateModelTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class DefaultStateModelTests: XCTestCase {

    func test_defaultStateModel_isAccordingTitleParameter() {
        
        let title = "Any title"
        let sut = DefaultStateModel(withTitle: title)
        
        XCTAssertEqual(sut.type, .default)
        XCTAssertEqual(sut.title, title)
    }

}
