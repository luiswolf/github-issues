//
//  OpenStateModelTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class OpenStateModelTests: XCTestCase {

    func test_openStateModel_isIgnoringTitleParameter() {
        
        let title = "Any title"
        let sut = OpenStateModel(withTitle: title)
        
        XCTAssertEqual(sut.type, .open)
        XCTAssertNotEqual(sut.title, title)
    }

}
