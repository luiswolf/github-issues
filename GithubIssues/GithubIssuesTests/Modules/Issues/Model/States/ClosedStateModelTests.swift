//
//  ClosedStateModelTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class ClosedStateModelTests: XCTestCase {

    func test_closedStateModel_isIgnoringTitleParameter() {
        
        let title = "Any title"
        let sut = ClosedStateModel(withTitle: title)
        
        XCTAssertEqual(sut.type, .closed)
        XCTAssertNotEqual(sut.title, title)
    }

}
