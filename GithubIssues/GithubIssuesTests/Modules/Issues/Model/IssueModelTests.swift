//
//  IssueModelTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import XCTest

@testable import GithubIssues
class IssueModelTests: XCTestCase {

    func test_issueModel_isWorking() {
        let issue = IssuesTestHelper.closedIssue
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(issue) else {
            XCTFail("não foi possível fazer o encoding")
            return
        }
        
        let decoder = JSONDecoder()
        guard let decodedIssue = try? decoder.decode(IssueModel.self, from: data) else {
            XCTFail("não foi possível fazer o decoding")
            return
        }
        
        XCTAssertEqual(issue.id, decodedIssue.id)
        XCTAssertEqual(issue.title, decodedIssue.title)
        XCTAssertEqual(issue.stateType, decodedIssue.stateType)
        XCTAssertEqual(issue.state.title, decodedIssue.state.title)
        XCTAssertEqual(issue.description, decodedIssue.description)
        XCTAssertEqual(issue.url, decodedIssue.url)
        XCTAssertEqual(issue.date, decodedIssue.date)
        XCTAssertEqual(issue.user.avatarUrl, decodedIssue.user.avatarUrl)
    }
    

}
