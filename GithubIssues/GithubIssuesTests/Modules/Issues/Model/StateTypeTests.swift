//
//  StateTypeTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import XCTest

@testable import GithubIssues
class StateTypeTests: XCTestCase {

    func test_closedStateType_hasCorrectMetatype() {
        
        let sut: StateType = .closed
        
        XCTAssertTrue(sut.metatype == ClosedStateModel.self)
    }
    func test_openStateType_hasCorrectMetatype() {
        
        let sut: StateType = .open
        
        XCTAssertTrue(sut.metatype == OpenStateModel.self)
    }
    
    func test_defaultStateType_hasCorrectMetatype() {
        
        let sut: StateType = .default
        
        XCTAssertTrue(sut.metatype == DefaultStateModel.self)
    }

}
