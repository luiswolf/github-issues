//
//  IssuesViewModelTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest
import Core

@testable import GithubIssues
class IssuesViewModelTests: XCTestCase {
    
    let issue = IssuesTestHelper.closedIssue
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_whenObjectIsNil_defaultErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = IssuesViewModel(withProvider: provider)
        let delegate = IssuesViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 1)
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, IssuesConstant.Message.defaultError)
    }
    
    func test_whenObjectIsEmpty_noDataErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: [IssueModel]())
        let sut = IssuesViewModel(withProvider: provider)
        let delegate = IssuesViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 0)
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, IssuesConstant.Message.noDataError)
    }
    
    func test_whenObjectIsSet_dataIsLoaded() {
        let provider = MockNetworkingProvider(withModel: [issue])
        let sut = IssuesViewModel(withProvider: provider)
        let delegate = IssuesViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 1)
        XCTAssertEqual(delegate.gotData, true)
        XCTAssertEqual(sut.issues?.count, 1)
    }
    
    func test_whenTheresError_canLoadNextPage() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = IssuesViewModel(withProvider: provider)
        let delegate = IssuesViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        sut.nextPage()
        XCTAssertEqual(provider.performCount, 2)
    }
    
    func test_whenObjectIsEmpty_canNotLoadNextPage() {
        let provider = MockNetworkingProvider(withModel: [IssueModel]())
        let sut = IssuesViewModel(withProvider: provider)
        let delegate = IssuesViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        sut.nextPage()
        XCTAssertEqual(provider.performCount, 1)
    }
    
    func test_whenObjectIsSet_canLoadNextPage() {
        let provider = MockNetworkingProvider(withModel: [issue])
        let sut = IssuesViewModel(withProvider: provider)
        let delegate = IssuesViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        sut.nextPage()
        XCTAssertEqual(provider.performCount, 2)
    }
    
}

class IssuesViewModelTestsDelegate: IssuesViewModelDelegate {

    var gotData: Bool = false
    var error: String?
    
    func didGetData() {
        gotData = true
    }
    
    func didGetError(withMessage message: String) {
        error = message
    }
}
    
class MockNetworkingProvider: NetworkingProviderProtocol {
    
    var performCount: Int = 0
    var model: [IssueModel]?
    
    init(withModel model: [IssueModel]?) {
        self.model = model
    }
    
    func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
        performCount += 1
        callback(model as? T)
    }
    
    func download(fromUrl url: String, returningTo callback: @escaping DataCallback) {
        
    }
    
}
