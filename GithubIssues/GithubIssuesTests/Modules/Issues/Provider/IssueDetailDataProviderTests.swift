//
//  IssueDetailDataProviderTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class IssueDetailDataProviderTests: XCTestCase {

    var dataProvider: IssueDetailDataProvider!
    var tableView: UITableView!
    var issue: IssueModel!
    
    override func setUp() {
        super.setUp()
        issue = IssuesTestHelper.closedIssue
        dataProvider = IssueDetailDataProvider(withIssue: issue)
        tableView = UITableView()
        tableView.dataSource = dataProvider
        tableView.delegate = dataProvider
    }
    
    override func tearDown() {
        super.tearDown()
        dataProvider = nil
        tableView = nil
        issue = nil
    }
    
    func test_numberOfRows_WhenDescriptionIsEmpty_isTwo() {
        let emptyDescriptionIssue = IssuesTestHelper.closedIssueWithoutDescription
        dataProvider = IssueDetailDataProvider(withIssue: emptyDescriptionIssue)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 2)
    }
    
    func test_numberOfRows_WhenDescriptionIsNotEmpty_isThree() {
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 3)
    }
    
    func test_titleOfSectionHeader_isBlankSpace() {
        XCTAssertEqual(dataProvider.tableView(tableView, titleForHeaderInSection: 0), " ")
    }
    
    func test_heightForRow_isAutomaticDimension() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForRowAt: IndexPath(row: 0, section: 0)), UITableView.automaticDimension)
    }
    
    func test_heightForHeaderInSection_isEight() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForHeaderInSection: 0), 8.0)
    }
    
    func test_firstCell_isHeaderCellType() {
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))

        XCTAssertTrue(cell is HeaderTableViewCell, "primeira célula é do tipo HeaderTableViewCell")
    }
    
    func test_secondCell_isStateCellType() {
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))

        XCTAssertTrue(cell is StateTableViewCell, "primeira célula é do tipo StateTableViewCell")
    }
    
    func test_thirdCell_isStateCellType() {
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 2, section: 0))

        XCTAssertTrue(cell is BasicTableViewCell, "primeira célula é do tipo BasicTableViewCell")
    }
    
}
