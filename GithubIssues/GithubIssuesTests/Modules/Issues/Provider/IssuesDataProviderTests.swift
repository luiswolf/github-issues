//
//  IssuesDataProviderTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
@testable import Core

class IssuesDataProviderDelegateMock: IssuesDataProviderDelegate {
    
    var didSelectCell: Bool = false
    var shouldGetMoreRowsValue: Bool = false
    var selectedIssue: IssueModel?
    
    func didSelect(item: IssueModel) {
        didSelectCell = true
        selectedIssue = item
    }
    
    func shouldGetMoreRows() {
        shouldGetMoreRowsValue = true
    }
    
}

class IssuesDataProviderTests: XCTestCase {

    var dataProvider: IssuesDataProvider!
    var tableView: UITableView!
    var selectionDelegate: IssuesDataProviderDelegateMock!
    private lazy var issue: IssueModel = {
        IssueModel(
            id: 123,
            title: "Test",
            stateType: .closed,
            state: ClosedStateModel(withTitle: "closed"),
            description: "Description",
            url: "https://teste.com",
            date: Date(),
            user: IssueModel.User(
                avatarUrl: "https://avatar.com"
            )
        )
    }()
    
    override func setUp() {
        super.setUp()
        
        tableView = UITableView()
        selectionDelegate = IssuesDataProviderDelegateMock()
        dataProvider = IssuesDataProvider(withTableView: tableView)
        dataProvider.delegate = selectionDelegate
    }
    
    override func tearDown() {
        super.tearDown()
        
        tableView = nil
        dataProvider = nil
    }

    func test_numberOfRows_isZeroAtStart() {
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 0)
    }
    
    func test_numberOfRows_isIssuesCountWhenPaginationIsZero() {
        dataProvider.set(issues: [issue, issue], andPaginationRows: 0)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 2)
    }
    
    func test_numberOfRows_isIssuesCountWhenPaginationIsNotZero() {
        dataProvider.set(issues: [issue, issue], andPaginationRows: 1)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 3)
    }
    
    func test_titleOfSectionHeader_isBlankSpace() {
        XCTAssertEqual(dataProvider.tableView(tableView, titleForHeaderInSection: 0), " ")
    }
    
    func test_heightForRow_isAutomaticDimension() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForRowAt: IndexPath(row: 0, section: 0)), UITableView.automaticDimension)
    }
    
    func test_heightForHeaderInSection_isEight() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForHeaderInSection: 0), 8.0)
    }
    
    func test_cell_isNotSelectedAtStart() {
        dataProvider.set(issues: [issue, issue], andPaginationRows: 1)
        XCTAssertEqual(selectionDelegate.didSelectCell, false)
    }
    
    func test_cell_isNotSelectedOnActionIfTheresNoIssuesSet() {
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.didSelectCell, false)
    }
    
    func test_cell_isSelectedOnAction() {
        dataProvider.set(issues: [issue, issue], andPaginationRows: 1)
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.didSelectCell, true)
    }
    
    func test_selectionIssue_hasRightTitle() {
        dataProvider.set(issues: [issue, issue], andPaginationRows: 1)
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.selectedIssue?.title, "Test")
    }
    
    func test_issueCell_isLoaded() {
        dataProvider.set(issues: [issue], andPaginationRows: 0)
        
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is IssueTableViewCell)
    }
    
    func test_loaderCell_isLoaded() {
        dataProvider.set(issues: [issue], andPaginationRows: 1)
        
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertTrue(cell is LoaderTableViewCell)
    }
    
    func test_paginationMethod_isNotCalled() {
        dataProvider.set(issues: [issue], andPaginationRows: 0)
        
        _ = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.shouldGetMoreRowsValue, false)
    }
    
    func test_paginationMethod_isCalled() {
        dataProvider.set(issues: [issue], andPaginationRows: 1)
        
        _ = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertEqual(selectionDelegate.shouldGetMoreRowsValue, true)
    }
    
}
