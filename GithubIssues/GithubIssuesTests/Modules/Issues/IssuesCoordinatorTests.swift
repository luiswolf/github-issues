//
//  IssuesCoordinatorTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class IssuesCoordinatorTests: XCTestCase {

    var nc: UINavigationController!
    var sut: IssuesCoordinator!
    
    override func setUp() {
        super.setUp()
        nc = UINavigationController()
        sut = IssuesCoordinator(navigationController: nc)
    }
    
    override func tearDown() {
        super.tearDown()
        nc = nil
        sut = nil
    }
    
    func test_beforeStart_shouldHaveNoViewControllers() {
        XCTAssertEqual(nc.viewControllers.count, 0)
    }
    
    func test_afterStart_issuesTableViewControllerIsTopViewController() {
        sut.start()
        XCTAssertTrue(nc.topViewController is IssuesTableViewController)
    }
    
    func test_onDetail_issueDetailTableViewControllerIsTopViewController() {
        sut.detail(ofIssue: IssuesTestHelper.openIssue)
        XCTAssertTrue(nc.topViewController is IssueDetailTableViewController)
        
    }
    
}
