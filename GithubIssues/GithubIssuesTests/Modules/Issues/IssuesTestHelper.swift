//
//  IssuesTestHelper.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import Foundation

@testable import GithubIssues
struct IssuesTestHelper {
    
    static let apiUrl = IssuesConstant.Endpoint.issues
        .replacingOccurrences(of: "{pageSize}", with: String("%d"))
        .replacingOccurrences(of: "{pageNumber}", with: String("%d"))
    
    static let closedIssue = IssueModel(
        id: 123,
        title: "Test",
        stateType: .closed,
        state: ClosedStateModel(withTitle: "closed"),
        description: "Description",
        url: "https://teste.com",
        date: Date(),
        user: IssueModel.User(
            avatarUrl: "https://avatar.com"
        )
    )
    
    static let closedIssueWithoutDescription = IssueModel(
        id: 123,
        title: "Test",
        stateType: .closed,
        state: ClosedStateModel(withTitle: "closed"),
        description: "",
        url: "https://teste.com",
        date: Date(),
        user: IssueModel.User(
            avatarUrl: "https://avatar.com"
        )
    )
    
    static let openIssue = IssueModel(
        id: 123,
        title: "Test",
        stateType: .open,
        state: ClosedStateModel(withTitle: "open"),
        description: "Description",
        url: "https://teste.com",
        date: Date(),
        user: IssueModel.User(
            avatarUrl: "https://avatar.com"
        )
    )
    
}
