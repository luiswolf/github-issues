//
//  IssueDetailTableViewControllerTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest
import Core

@testable import GithubIssues
class IssueDetailTableViewControllerTests: XCTestCase {
    
    var sut: IssueDetailTableViewController!
    var nc: UINavigationController!
    let issue = IssuesTestHelper.closedIssue
    
    override func setUp() {
        super.setUp()
        sut = IssueDetailTableViewController(withIssue: issue)
        nc = UINavigationController(rootViewController: sut)
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
        nc = nil
    }

    func test_InitWithCoder_isNil() {
        let vc = IssueDetailTableViewController(coder: NSCoder())
        XCTAssertNil(vc)
    }
    
    func test_title_isSetAfterViewDidLoad() {
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.title, "Issue #\(issue.id)")
    }
    
    func test_toolbar_hasThreeOptions() {
        XCTAssertEqual(sut.toolbarItems?.count, 3)
    }
    
    func test_toolbar_isVisibleAfterWillAppear() {
        sut.loadViewIfNeeded()
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        XCTAssertEqual(nc.isToolbarHidden, false)
    }
    
    func test_toolbar_isVisibleAfterWillDisappear() {
        sut.loadViewIfNeeded()
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        sut.viewWillDisappear(false)
        XCTAssertEqual(nc.isToolbarHidden, true)
    }
    
}
