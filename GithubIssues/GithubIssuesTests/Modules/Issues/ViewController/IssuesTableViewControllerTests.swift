//
//  IssuesTableViewControllerTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest
import Core

@testable import GithubIssues
class IssuesTableViewControllerTests: XCTestCase {

    var sut: IssuesTableViewController!
    var viewModel: IssuesViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = IssuesViewModel()
        sut = IssuesTableViewController(withViewModel: viewModel)
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func test_InitWithCoder_isNil() {
        let vc = IssuesTableViewController(coder: NSCoder())
        XCTAssertNil(vc)
    }
    
    func test_title_isSetAfterViewDidLoad() {
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.title, IssuesConstant.Title.issues)
    }
    
    func test_tableView_hasIssuesDataProvider() {
        sut.loadViewIfNeeded()
        XCTAssertTrue(sut.tableView.delegate is IssuesDataProvider)
        XCTAssertTrue(sut.tableView.dataSource is IssuesDataProvider)
    }
    
    func test_data_isSameOnTableViewAndViewModel() {
        sut.loadViewIfNeeded()
        sut.didGetData()
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.issues?.count ?? 0) + viewModel.paginationRows()
        
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_errorView_isVisibleOnError() {
        sut.loadViewIfNeeded()
        sut.didGetError(withMessage: "Teste")
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
        
}
