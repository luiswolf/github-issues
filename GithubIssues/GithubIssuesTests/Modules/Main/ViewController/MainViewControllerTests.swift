//
//  MainViewControllerTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class MainViewControllerTests: XCTestCase {

    func test_childCoordinatorsIsIncreased_afterContinue() {
        let coordinator = MainCoordinator(navigationController: UINavigationController())
        let vc = MainViewController()
        vc.coordinator = coordinator
        vc.didContinue()
        
        XCTAssertEqual(coordinator.childCoordinators.count, 1)
    }

}
