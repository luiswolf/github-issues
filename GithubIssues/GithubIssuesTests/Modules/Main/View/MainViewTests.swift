//
//  MainViewTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class MainViewTests: XCTestCase {

    func test_InitWithCoder_isNil() {
        let view = MainView(coder: NSCoder())
        XCTAssertNil(view)
    }

}
