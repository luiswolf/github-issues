//
//  MainCoordinatorTests.swift
//  GithubIssuesTests
//
//  Created by Luis Emilio Dias Wolf on 10/03/21.
//

import XCTest

@testable import GithubIssues
class MainCoordinatorTests: XCTestCase {

    var nc: UINavigationController!
    var sut: MainCoordinator!
    
    override func setUp() {
        super.setUp()
        nc = UINavigationController()
        sut = MainCoordinator(navigationController: nc)
    }
    
    override func tearDown() {
        super.tearDown()
        nc = nil
        sut = nil
    }
    
    func test_beforeStart_shouldHaveNoViewControllers() {
        XCTAssertEqual(nc.viewControllers.count, 0)
    }
    
    func test_afterStart_mainViewControllerIsTopViewController() {
        sut.start()
        XCTAssertTrue(nc.topViewController is MainViewController)
    }
    
    func test_onIssues_issuesTableViewControllerIsTopViewController() {
        sut.issues()
        XCTAssertTrue(nc.topViewController is IssuesTableViewController)
        
    }
}
