//
//  AppDelegate.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coordinator: CoordinatorProtocol?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let nc = UINavigationController()
        coordinator = MainCoordinator(navigationController: nc)
        coordinator?.start()
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = nc
        window.makeKeyAndVisible()
        window.tintColor = .black
        
        self.window = window
        return true
    }

}
