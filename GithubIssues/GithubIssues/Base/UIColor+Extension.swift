//
//  UIColor+Extension.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

extension UIColor {
    
    static let black = UIColor(named: "Black") ?? .black
    static let gray = UIColor(named: "Gray") ?? .darkGray
    static let green = UIColor(named: "Green") ?? .systemGreen
    static let purple = UIColor(named: "Purple") ?? .purple
    static let orange = UIColor(named: "Orange") ?? .orange
    static let white = UIColor(named: "White") ?? .white
    
}
