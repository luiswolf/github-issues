//
//  MainCoordinator.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

class MainCoordinator: CoordinatorProtocol {
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = MainViewController()
        vc.coordinator = self
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    
}

// MARK: - Actions
extension MainCoordinator {
    
    func issues() {
        let issues = IssuesCoordinator(navigationController: navigationController)
        childCoordinators.append(issues)
        issues.start()
    }

}
