//
//  MainView.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

class MainView: UIView {
    
    private typealias Constant = MainConstant
    
    weak var delegate: MainViewDelegate?

    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        view.addSubview(button)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.spacing = 24
        view.addArrangedSubview(imageView)
        view.addArrangedSubview(titleLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Constant.Title.app
        label.font = .systemFont(ofSize: 17.0)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let image = UIImage(named: Constant.Image.logo)
        let view = UIImageView(image: image)
        view.contentMode = .scaleAspectFit
        view.tintColor = .black
        return view
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.setTitle(Constant.Title.continue, for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(`continue`), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Setup
extension MainView {
    
    private func commonInit() {
        addSubview(contentView)
        autoLayout()
    }

    private func autoLayout() {
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 128),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            stackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16.0),
            button.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -24.0),
            button.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
}

// MARK: - Actions
extension MainView {
    
    @objc
    private func `continue`(){
        delegate?.didContinue()
    }
    
}
