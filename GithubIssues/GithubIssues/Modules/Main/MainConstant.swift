//
//  MainConstant.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

struct MainConstant {
    
    enum Image {
        static let logo = "logo"
    }
    
    enum Title {
        static let `continue` = "Continuar"
        static let app = "Swift issues"
    }
    
}
