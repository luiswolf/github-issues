//
//  MainViewController.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

class MainViewController: UIViewController {

    weak var coordinator: MainCoordinator?
    
    private lazy var mainView: MainView = {
        let view = MainView()
        view.delegate = self
        return view
    }()

}

// MARK: - Lifecycle
extension MainViewController {
    
    override func loadView() {
        view = mainView
    }
    
}

// MARK: - MainViewDelegate
extension MainViewController: MainViewDelegate {
    
    func didContinue() {
        coordinator?.issues()
    }
    
}
