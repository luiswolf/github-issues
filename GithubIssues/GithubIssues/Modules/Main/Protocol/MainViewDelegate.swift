//
//  MainViewDelegate.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

protocol MainViewDelegate: class {
    
    func didContinue()
    
}
