//
//  IssueDetailDataProvider.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import UIKit

class IssueDetailDataProvider: NSObject {
    
    private typealias BasicCell = BasicTableViewCell
    private typealias StateCell = StateTableViewCell
    private typealias HeaderCell = HeaderTableViewCell
    
    private let issue: IssueModel
    private var rows = [UITableViewCell]()
    
    init(withIssue issue: IssueModel) {
        self.issue = issue
        super.init()
        self.configure()
    }
    
    private func configure() {
        rows.append(HeaderCell(withIssue: issue))
        rows.append(StateCell(withState: issue.state))
        if !issue.description.isEmpty {
            rows.append(BasicCell(withLabel: issue.description))
        }
    }
    
}

// MARK: - UITableViewDelegate
extension IssueDetailDataProvider: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    
}

// MARK: - UITableViewDataSource
extension IssueDetailDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = rows[indexPath.row]
        cell.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        return cell
    }
    
}
