//
//  IssuesDataProvider.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import UIKit
import Core

class IssuesDataProvider: NSObject {
    
    private typealias IssueCell = IssueTableViewCell
    private typealias LoaderCell = LoaderTableViewCell
    
    weak var delegate: IssuesDataProviderDelegate?
    weak var tableView: UITableView?
    
    private var issues: [IssueModel]?
    private var paginationRows: Int = 0
    
    func set(issues: [IssueModel]?, andPaginationRows paginationRows: Int) {
        self.issues = issues
        self.paginationRows = paginationRows
    }
    
    private func register() {
        tableView?.register(IssueCell.self, forCellReuseIdentifier: IssueCell.identifier)
        tableView?.register(LoaderCell.self, forCellReuseIdentifier: LoaderCell.identifier)
    }
    
    init(withTableView tableView: UITableView?) {
        super.init()
        self.tableView = tableView
        self.register()
    }
    
}

// MARK: - UITableViewDelegate
extension IssuesDataProvider: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let issues = issues else { return }
        let issue = issues[indexPath.row]
        delegate?.didSelect(item: issue)
    }
    
}

// MARK: - UITableViewDataSource
extension IssuesDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let issues = issues else { return 0 }
        return issues.count + paginationRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let issues = issues else {
            return UITableViewCell() }
        guard indexPath.row == issues.count else {
            let issue = issues[indexPath.row]
            return cell(forIssue: issue, atIndexPath: indexPath) ?? UITableViewCell()
        }
        delegate?.shouldGetMoreRows()
        return cell(forLoaderAtIndexPath: indexPath) ?? UITableViewCell()
    }
    
}

// MARK: - Helper
extension IssuesDataProvider {
    
    private func cell(forIssue issue: IssueModel,  atIndexPath indexPath: IndexPath) -> IssueCell? {
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: IssueCell.identifier, for: indexPath) as? IssueCell
        cell?.configure(withIssue: issue)
        return cell
    }
    
    private func cell(forLoaderAtIndexPath indexPath: IndexPath) -> LoaderCell? {
        return self.tableView?.dequeueReusableCell(withIdentifier: LoaderCell.identifier, for: indexPath) as? LoaderCell
    }
    
}
