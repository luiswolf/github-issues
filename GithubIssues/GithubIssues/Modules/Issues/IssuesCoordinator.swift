//
//  IssuesCoordinator.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

class IssuesCoordinator: CoordinatorProtocol {
    
    private typealias Constant = IssuesConstant
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = IssuesTableViewController()
        vc.coordinator = self
        let backButton = UIBarButtonItem(
            title: String(),
            style: .plain,
            target: nil,
            action: nil
        )
        vc.navigationItem.leftBarButtonItem = backButton
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationController.pushViewController(vc, animated: true)
    }
    
}

// MARK: - Actions
extension IssuesCoordinator {
    
    func detail(ofIssue issue: IssueModel) {
        let vc = IssueDetailTableViewController(withIssue: issue)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func open(_ url: URL) {
        let alertController = UIAlertController(
            title: Constant.Title.caution,
            message: Constant.Message.redirect,
            preferredStyle: .alert
        )
        let defaultAction = UIAlertAction(
            title: Constant.Title.yes,
            style: .default
        ) { (action) in
            UIApplication.shared.open(url)
        }
        let cancelAction = UIAlertAction(
            title: Constant.Title.cancel,
            style: .cancel
        )
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        navigationController.present(alertController, animated: true)
    }

}
