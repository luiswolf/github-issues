//
//  IssuesTableViewController.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

class IssuesTableViewController: UITableViewController, LoaderProtocol, ErrorProtocol {

    private typealias Constant = IssuesConstant
    
    weak var coordinator: IssuesCoordinator?
    
    private let viewModel: IssuesViewModel
    private lazy var dataProvider: IssuesDataProvider = {
        let provider = IssuesDataProvider(withTableView: tableView)
        provider.delegate = self
        return provider
    }()
    
    init(withViewModel viewModel: IssuesViewModel = IssuesViewModel()) {
        self.viewModel = viewModel
        super.init(style: .grouped)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        return nil
    }

}

// MARK: - Lifecycle
extension IssuesTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Constant.Title.issues
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
        showLoader()
        viewModel.fetch()
    }
    
}

// MARK: - IssuesDataProviderDelegate
extension IssuesTableViewController: IssuesDataProviderDelegate {
    
    func shouldGetMoreRows() {
        viewModel.nextPage()
    }
    
    func didSelect(item: IssueModel) {
        coordinator?.detail(ofIssue: item)
    }
    
}

// MARK: - IssuesViewModelDelegate
extension IssuesTableViewController: IssuesViewModelDelegate {
    
    func didGetData() {
        hideLoader()
        dataProvider.set(issues: viewModel.issues, andPaginationRows: viewModel.paginationRows())
        tableView.reloadData()
    }
    
    func didGetError(withMessage message: String) {
        hideLoader()
        showError(withMessage: message)
    }
    
}
