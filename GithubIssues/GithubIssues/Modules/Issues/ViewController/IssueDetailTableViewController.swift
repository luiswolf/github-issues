//
//  IssueDetailTableViewController.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

class IssueDetailTableViewController: UITableViewController {

    private typealias Constant = IssuesConstant
    
    weak var coordinator: IssuesCoordinator?
    
    private let issue: IssueModel
    
    private lazy var dataProvider: IssueDetailDataProvider = {
        return IssueDetailDataProvider(withIssue: issue)
    }()
    
    internal override var toolbarItems: [UIBarButtonItem]? {
        set {}
        get {[
            UIBarButtonItem(
                barButtonSystemItem: .flexibleSpace,
                target: nil,
                action: nil
            ),
            UIBarButtonItem(
                title: Constant.Title.seeOnGithub,
                style: .plain,
                target: self,
                action: #selector(open)
            ),
            UIBarButtonItem(
                barButtonSystemItem: .flexibleSpace,
                target: nil,
                action: nil
            )
        ]}
    }
    
    init(withIssue issue: IssueModel) {
        self.issue = issue
        super.init(style: .grouped)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Lifecycle
extension IssueDetailTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = String(format: Constant.Title.issueDetail, arguments: [issue.id])
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setToolbarHidden(true, animated: true)
    }
    
}

// MARK: - Actions
extension IssueDetailTableViewController {
    
    @objc
    private func open() {
        guard let url = URL(string: issue.url) else { return }
        coordinator?.open(url)
    }
    
}
