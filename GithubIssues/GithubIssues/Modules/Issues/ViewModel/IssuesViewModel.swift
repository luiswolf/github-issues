//
//  IssuesViewModel.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation
import Core

class IssuesViewModel: NSObject {
    
    weak var delegate: IssuesViewModelDelegate?
    
    private(set) var issues: [IssueModel]?
    
    private var canLoadNextPage: Bool = false
    private let neworkingProvider: NetworkingProviderProtocol
    private lazy var issuesRequest: IssuesRequest = {
        let request = IssuesRequest()
        return request
    }()
    
    init(withProvider provider: NetworkingProviderProtocol = AppNetworkingProvider()) {
        self.neworkingProvider = provider
    }
    
}

// MARK: - Networking
extension IssuesViewModel {
    
    func fetch() {
        neworkingProvider.perform(issuesRequest) { [weak self] (response: [IssueModel]?) in
            guard let self = self else { return }
            
            guard let response = response else {
                self.canLoadNextPage = true
                self.issuesRequest.previousPage()
                self.delegate?.didGetError(withMessage: IssuesConstant.Message.defaultError)
                return
            }
            
            guard !response.isEmpty else {
                self.canLoadNextPage = false
                guard self.issues == nil else {
                    self.delegate?.didGetData()
                    return
                }
                self.delegate?.didGetError(withMessage: IssuesConstant.Message.noDataError)
                return
            }
            
            self.canLoadNextPage = true
            self.issues = self.issues ?? []
            self.issues?.append(contentsOf: response)
            self.delegate?.didGetData()
        }
    }
    
}

// MARK: - Pagination
extension IssuesViewModel {
    
    func nextPage() {
        guard canLoadNextPage else { return }
        canLoadNextPage = false
        issuesRequest.nextPage()
        fetch()
    }
    
    func paginationRows() -> Int {
        guard canLoadNextPage else { return 0 }
        return 1
    }
    
}
