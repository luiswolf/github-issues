//
//  IssuesConstant.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

struct IssuesConstant {
    
    enum Endpoint {
        static let issues = "https://api.github.com/repos/apple/swift/issues?per_page={pageSize}&page={pageNumber}&state=all"
    }
    
    enum Image {
        static let avatar = "avatar"
    }
    
    enum Title {
        static let cancel = "Cancelar"
        static let caution = "Atenção"
        static let closed = "fechado"
        static let issues = "Swift Issues"
        static let issueDetail = "Issue #%d"
        static let open = "aberto"
        static let seeOnGithub = "Ver no Github"
        static let state = "Situação"
        static let yes = "Sim"
    }
    
    enum Message {
        static let defaultError = "Não foi possível consultar issues. Tente novamente mais tarde."
        static let noDataError = "Não há issues para exibir."
        static let redirect = "Você será redirecionado para o site do Github. Deseja continuar?"
    }
    
}
