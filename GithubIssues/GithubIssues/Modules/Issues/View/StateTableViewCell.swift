//
//  StateTableViewCell.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

class StateTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: StateTableViewCell.self)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16.0)
        label.text = IssuesConstant.Title.state
        label.numberOfLines = 1
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var stateLabel: UILabel = {
        let label = StateLabel()
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fill
        view.alignment = .fill
        view.spacing = 4.0
        view.addArrangedSubview(titleLabel)
        view.addArrangedSubview(stateLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    init(withState state: StateModelProtocol) {
        super.init(style: .default, reuseIdentifier: nil)
        self.stateLabel.text = state.title
        self.stateLabel.textColor = state.foregroundColor
        self.stateLabel.backgroundColor = state.backgroundColor
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
}

// MARK: - Setup
extension StateTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(stackView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let constraints = [
            stackView.topAnchor.constraint(equalTo: margin.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}
