//
//  HeaderTableViewCell.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

class HeaderTableViewCell: UITableViewCell {
    
    private let imageSize: CGFloat = 128.0
    
    private lazy var imageHelper: ImageHelper = {
        return ImageHelper(withProvider: AppNetworkingProvider())
    }()
    
    private lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        return formatter
    }()
    
    static let identifier = String(describing: HeaderTableViewCell.self)

    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .center
        view.spacing = 16.0
        view.addArrangedSubview(avatarView)
        view.addArrangedSubview(labelStackView)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var labelStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .center
        view.spacing = 4.0
        view.addArrangedSubview(title)
        view.addArrangedSubview(detail)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    internal lazy var avatarView: UIImageView = {
        let image = UIImage(named: IssuesConstant.Image.avatar)
        let view = UIImageView(image: image)
        view.tintColor = .black
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = false
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.cornerRadius = imageSize / 2
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 16.0)
        label.numberOfLines = 0
        label.textColor = .gray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var detail: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    init(withIssue issue: IssueModel) {
        super.init(style: .default, reuseIdentifier: nil)
        commonInit()
        imageHelper.get(imageWithPath: issue.user.avatarUrl) { image in
            self.avatarView.image = image
        }
        title.text = issue.title
        detail.text = formatter.string(from: issue.date)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
}

// MARK: - Setup
extension HeaderTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(mainStackView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let constraints = [
            mainStackView.topAnchor.constraint(equalTo: margin.topAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            avatarView.widthAnchor.constraint(equalToConstant: imageSize),
            avatarView.heightAnchor.constraint(equalTo: avatarView.widthAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}
