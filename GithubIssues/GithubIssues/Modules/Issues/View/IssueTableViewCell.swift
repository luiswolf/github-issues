//
//  IssueTableViewCell.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit
import Core

class IssueTableViewCell: UITableViewCell {

    static let identifier = String(describing: IssueTableViewCell.self)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 16.0)
        label.numberOfLines = 0
        label.textColor = .gray
        return label
    }()
    
    private lazy var stateLabel: UILabel = {
        let label = StateLabel()
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .leading
        view.spacing = 4.0
        view.addArrangedSubview(titleLabel)
        view.addArrangedSubview(stateLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
}

// MARK: - Setup
extension IssueTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(stackView)
        accessoryType = .disclosureIndicator
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let constraints = [
            stackView.topAnchor.constraint(equalTo: margin.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func configure(withIssue issue: IssueModel) {
        titleLabel.text = issue.title
        stateLabel.text = issue.state.title
        stateLabel.textColor = issue.state.foregroundColor
        stateLabel.backgroundColor = issue.state.backgroundColor
    }
    
}
