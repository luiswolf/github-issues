//
//  BasicTableViewCell.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

class BasicTableViewCell: UITableViewCell {

    static let identifier = String(describing: BasicTableViewCell.self)

    private lazy var label: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16.0)
        label.numberOfLines = 0
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    init(withLabel label: String) {
        super.init(style: .default, reuseIdentifier: nil)
        self.label.text = label
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
}

// MARK: - Setup
extension BasicTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(label)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        
        let bottomConstraint = label.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        bottomConstraint.priority = .fittingSizeLevel
        let constraints = [
            label.topAnchor.constraint(equalTo: margin.topAnchor),
            bottomConstraint,
            label.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            label.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}
