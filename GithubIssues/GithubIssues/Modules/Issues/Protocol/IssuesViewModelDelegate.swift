//
//  IssuesViewModelDelegate.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

protocol IssuesViewModelDelegate: class {
    
    func didGetData()
    
    func didGetError(withMessage message: String)
    
}
