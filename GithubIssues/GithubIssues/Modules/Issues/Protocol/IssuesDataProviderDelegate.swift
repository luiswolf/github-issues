//
//  IssuesDataProviderDelegate.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import Foundation

protocol IssuesDataProviderDelegate: class {
    
    func didSelect(item: IssueModel)
    
    func shouldGetMoreRows()
    
}
