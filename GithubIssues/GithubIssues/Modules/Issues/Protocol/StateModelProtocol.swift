//
//  StateModelProtocol.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

protocol StateModelProtocol {
    
    var type: StateType { get }
    
    var title: String { get }
    
    var backgroundColor: UIColor { get }
    
    var foregroundColor: UIColor { get }
    
    init(withTitle title: String)
    
}
