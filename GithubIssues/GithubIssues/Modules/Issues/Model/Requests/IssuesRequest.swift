//
//  IssuesRequest.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation
import Core

struct IssuesRequest: NetworkingRequestProtocol {
    
    private(set) var pageNumber: Int = 1
    private(set) var pageSize: Int = 20
    
    var method: NetworkingMethod { .get }
    
    var url: String {
        IssuesConstant.Endpoint.issues
            .replacingOccurrences(of: "{pageSize}", with: String(pageSize))
            .replacingOccurrences(of: "{pageNumber}", with: String(pageNumber))
    }
    
    var decoder: JSONDecoder { DateDecoder() }
    
}

// MARK: - Helper
extension IssuesRequest {
    
    mutating func nextPage() {
        pageNumber += 1
    }
    
    mutating func previousPage() {
        guard pageNumber > 1 else { return }
        pageNumber -= 1
    }
    
}
