//
//  IssueModel.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

struct IssueModel: Codable {
    
    struct User: Codable {
        let avatarUrl: String
        private enum CodingKeys: String, CodingKey {
            case avatarUrl = "avatar_url"
        }
    }
    
    let id: Int
    let title: String
    let stateType: StateType
    let state: StateModelProtocol
    let description: String
    let url: String
    let date: Date
    let user: User
    
    private enum CodingKeys: String, CodingKey {
        case id = "number"
        case title
        case stateType = "state"
        case description = "body"
        case url = "html_url"
        case date = "created_at"
        case user
    }
    
}

extension IssueModel {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        description = try container.decode(String.self, forKey: .description)
        url = try container.decode(String.self, forKey: .url)
        date = try container.decode(Date.self, forKey: .date)
        user = try container.decode(User.self, forKey: .user)
        stateType = try container.decode(StateType.self, forKey: .stateType)
        state = stateType.metatype.init(withTitle: title)
    }
    
}
