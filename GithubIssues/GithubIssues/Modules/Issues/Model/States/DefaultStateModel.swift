//
//  DefaultStateModel.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

struct DefaultStateModel: StateModelProtocol {
    
    var type: StateType { .default }
    
    var title: String
    
    var backgroundColor: UIColor { .lightGray }
    
    var foregroundColor: UIColor { .gray }
    
    init(withTitle title: String) {
        self.title = title
    }
    
}
