//
//  ClosedStateModel.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

struct ClosedStateModel: StateModelProtocol {
    
    var type: StateType { .closed }
    
    var title: String { IssuesConstant.Title.closed }
    
    var backgroundColor: UIColor { .purple }
    
    var foregroundColor: UIColor { .white }
    
    init(withTitle title: String) {}
    
}
