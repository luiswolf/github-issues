//
//  OpenIssueStateModel.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

struct OpenStateModel: StateModelProtocol {
    
    var type: StateType { .open }
    
    var title: String { IssuesConstant.Title.open }
    
    var backgroundColor: UIColor { .green }
    
    var foregroundColor: UIColor { .white }
    
    init(withTitle title: String) {}
    
}
