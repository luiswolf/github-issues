//
//  StateType.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

enum StateType: String, Codable {
    
    case open
    case closed
    case `default`
    
    var metatype: StateModelProtocol.Type {
        switch self {
            case .open: return OpenStateModel.self
            case .closed: return ClosedStateModel.self
            case .default: return DefaultStateModel.self
        }
    }

}
