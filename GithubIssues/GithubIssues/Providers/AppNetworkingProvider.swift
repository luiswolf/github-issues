//
//  AppNetworkingProvider.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation
import Core
import Alamofire

class AppNetworkingProvider: NetworkingProviderProtocol {
    
    func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
        
        AF.request(
            request.url,
            method: request.method.alamofireMethod,
            encoding: URLEncoding.default
        ).responseDecodable(
            of: T.self,
            decoder: request.decoder
        ){ response in
            guard let data = response.value else {
                DispatchQueue.main.async {
                    callback(nil)
                }
                return
            }
            DispatchQueue.main.async {
                callback(data)
            }
        }
        
    }
    
    func download(fromUrl url: String, returningTo callback: @escaping DataCallback) {
        AF.download(url).responseData { response in
            guard case let Result.success(data) = response.result else {
                DispatchQueue.main.async {
                    callback(nil)
                }
                return
            }
            DispatchQueue.main.async {
                callback(data)
            }
        }
    }
    
}
