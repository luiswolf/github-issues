//
//  NetworkingMethod+Extension.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 08/03/21.
//

import Foundation
import Alamofire
import Core

extension NetworkingMethod {
    
    var alamofireMethod: HTTPMethod {
        switch self {
            case .get: return .get
            case .post: return .post
            case .put: return .put
        }
    }
    
}
