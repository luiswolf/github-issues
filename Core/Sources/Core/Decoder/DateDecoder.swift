//
//  DateDecoder.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

public class DateDecoder: JSONDecoder {
    
    public override init() {
        super.init()
        dateDecodingStrategy = .iso8601
    }
    
}
