//
//  NetworkingMethod.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

public enum NetworkingMethod {
    case get, post, put
}
