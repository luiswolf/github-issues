//
//  NetworkingRequestProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import Foundation

public protocol NetworkingRequestProtocol {
    
    var method: NetworkingMethod { get }
    
    var url: String { get }
    
    var decoder: JSONDecoder { get }
    
}

// MARK: - Default
extension NetworkingRequestProtocol {
    
    var decoder: JSONDecoder { JSONDecoder() }
    
}
