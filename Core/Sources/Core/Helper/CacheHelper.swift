//
//  CacheHelper.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 08/03/21.
//

import UIKit

final public class CacheHelper {
    
    private let image = NSCache<NSString, UIImage>()
    
    public static let shared = CacheHelper()
    
    private init() {}
    
    func getImage(forKey key: String) -> UIImage? {
        return image.object(forKey: NSString(string: key))
    }
    
    func storeImage(_ image: UIImage, forKey key: String) {
        guard getImage(forKey: key) == nil else { return }
        self.image.setObject(image, forKey: NSString(string: key))
    }
    
    func removeImage(forKey key: String) {
        self.image.removeObject(forKey: NSString(string: key))
    }
    
}
