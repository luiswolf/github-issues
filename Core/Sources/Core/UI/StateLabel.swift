//
//  StateLabel.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 07/03/21.
//

import UIKit

public class StateLabel: UILabel {

    let horizontalInset: CGFloat = 16.0
    let verticalInset: CGFloat = 4.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 5.0
        layer.masksToBounds = true
        font = .boldSystemFont(ofSize: 13.0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(
            top: verticalInset,
            left: horizontalInset,
            bottom: verticalInset,
            right: horizontalInset
        )
        super.drawText(in: rect.inset(by: insets))
    }

    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(
            width: size.width + (horizontalInset * 2),
            height: size.height + (verticalInset * 2)
        )
    }

    public override var bounds: CGRect {
        didSet {
            preferredMaxLayoutWidth = bounds.width - (verticalInset * 2)
        }
    }
    
}
