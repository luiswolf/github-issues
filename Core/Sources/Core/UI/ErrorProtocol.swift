//
//  ErrorProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import UIKit

public protocol ErrorProtocol: UIViewController {
    
    func showError(withMessage message: String)
    
    func hideError()
    
}

// MARK: - Default
extension ErrorProtocol {
    
    public func showError(withMessage message: String) {
        hideError()
    
        let container = ContainerView<ErrorView>()
        container.subview.message = message
        container.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(container)
        
        let margin = view.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: margin.topAnchor),
            container.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            container.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            container.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ])
    }
    
    public func hideError() {
        view.subviews.first(where: { $0 is ContainerView<ErrorView>})?.removeFromSuperview()
    }
    
}
