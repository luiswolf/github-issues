//
//  LoaderTableViewCell.swift
//  GithubIssues
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import UIKit

public class LoaderTableViewCell: UITableViewCell {

    public static let identifier = String(describing: LoaderTableViewCell.self)

    private lazy var indicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.startAnimating()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        indicatorView.startAnimating()
    }

}

// MARK: - Setup
extension LoaderTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(indicatorView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        
        let constraints = [
            indicatorView.topAnchor.constraint(equalTo: margin.topAnchor),
            indicatorView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            indicatorView.centerXAnchor.constraint(equalTo: margin.centerXAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}

