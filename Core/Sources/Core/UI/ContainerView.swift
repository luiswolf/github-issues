//
//  ContainerView.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 09/03/21.
//

import UIKit

public class ContainerView<T: UIView>: UIView {
    
    lazy var subview: T = {
        let view = T()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subview)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Setup
extension ContainerView {
    
    private func commonInit() {
        addSubview(contentView)
        autoLayout()
    }

    private func autoLayout() {
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            subview.centerYAnchor.constraint(equalTo: centerYAnchor),
            subview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0),
            subview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0)
        ])
    }
    
}
